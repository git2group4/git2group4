const express = require('express');
const body = require('body-parser')
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection');
const app = express();

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(body.urlencoded(express.static('public')));
app.use(cookie());
app.use(session({ secret: 'Passw0rd' }));
app.use(connection(mysql, {
    host: 'localhost',
    user: 'root',
    password: 'Alohomora',
    port: 3306,
    database: 'dbgroup4'
}, 'single'));

const homeRoute = require('./routes/homeRoute');
app.use('/', homeRoute);

const basRoute = require('./routes/s611998002Route');
app.use('/', basRoute);

const pramRoute = require('./routes/s611998005Route');
app.use('/', pramRoute);

const oatRoute = require('./routes/tb03Route');
app.use('/', oatRoute);

const tiewRoute = require('./routes/s611998019Route');
app.use('/', tiewRoute);


app.listen(8081);
