const { check } = require('express-validator');

exports.addValidator = [check('a611998002', "ชื่อไม่ถูกต้อง").not().isEmpty(),
    check('b611998002', "Flot ไม่ถูกต้อง").not().isEmpty(),
    check('c611998002', "กรอก ไม่ถูกต้อง").not().isEmpty(),
    check('d611998002', "Date ไม่ถูกต้อง").isDate()
];
exports.editValidator = [check('a611998002', "ชื่อไม่ถูกต้อง").not().isEmpty(),
    check('b611998002', "Flot ไม่ถูกต้อง").not().isEmpty(),
    check('c611998002', "กรอก ไม่ถูกต้อง").not().isEmpty(),
    check('d611998002', "Date ไม่ถูกต้อง").isDate()
];