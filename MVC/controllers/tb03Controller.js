const controller = {};

controller.list = (req,res) => {
  req.getConnection((err,conn) => {
    conn.query("SELECT * FROM tb03",(err,tb03)=>{
      if(err){
        res.json(err);
      }
      res.render('../views/tb03/tb03',{
        data:tb03
      });
    });
  });
};
controller.save = (req,res) => {
  const data = req.body;
  req.getConnection((err,conn) => {
    conn.query('insert into tb03 set ?',[data],(err,tb03)=>{
      if(err){
        res.json(err);
      }
      console.log('../views/tb03/tb03');
      res.redirect('/oat');
    });
  });
};
controller.delete = (req,res) => {
  const { id } = req.params;
  req.getConnection((err,conn) => {
    conn.query('delete from tb03 where id = ?',[id],(err,tb03)=>{
      if(err){
        res.json(err);
      }
      console.log('../views/tb03/tb03');
      res.redirect('/oat');
    });
  });
};
controller.edit = (req,res) => {
  const {id} = req.params;
  req.getConnection((err,conn) =>{
    conn.query('select * from tb03 where id = ?',[id],(err,tb03) =>{
      if(err){
        res.json(err);
      }
      res.render('../views/tb03/tb03Form',{
        data:tb03[0]
      });
    });
  });
};
controller.update = (req,res) => {
  const {id} = req.params;
  const data = req.body;
  req.getConnection((err,conn) =>{
    conn.query('update tb03 set ? where id = ?',[data,id],(err,tb03)=> {
      if(err){
        res.json(err);
      }
      res.redirect('/oat');
    });
  });
};
controller.new = (req,res)=>{
  const data = null;
  res.render('../views/tb03/tb03Form',{
    data:data
  });
};

module.exports = controller;