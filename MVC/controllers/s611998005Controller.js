const controller = {};

controller.list = (req,res) => {
  req.getConnection((err,conn) =>{
    conn.query('SELECT * FROM TB05', (err,s611998005) => {
      if(err){
        res.json(err);
      }
      res.render("../views/s611998005/s611998005",{
      data:s611998005
      });
    });
  });
};


controller.save = (req,res) => {
  const data = req.body;
  req.getConnection((err,conn) =>{
    conn.query('INSERT INTO TB05 SET ?',[data], (err,s611998005) => {
      if(err){
        res.json(err);
      }
      console.log(req.body);
      res.redirect('/pram');
    });
  });
};

controller.delete = (req,res) => {
  const {id} = req.params;
  req.getConnection((err,conn) =>{
    conn.query('DELETE FROM TB05 WHERE id = ?',[id], (err,s611998005) => {
      if(err){
        res.json(err);
      }
      console.log(s611998005);
      res.redirect('/pram');
    });
  });
};

controller.edit = (req,res) => {
  const {id} = req.params;
  req.getConnection((err,conn) =>{
    conn.query('SELECT * FROM TB05 WHERE id = ?',[id],(err,s611998005) =>{
      if(err){
        res.json(err);
      }

      res.render('../views/s611998005/s611998005Form',{
        data:s611998005[0]
      });
    });
  });
};

controller.update = (req,res) => {
  const {id} = req.params;
  const data = req.body;
  req.getConnection((err,conn) =>{
    conn.query('UPDATE TB05 SET ? WHERE id = ?',[data,id], (err,s611998005) => {
      if(err){
        res.json(err);
      }
    res.redirect('/pram')
    });
  });
};

controller.new = (req,res) =>{
  const data = null;
  res.render('../views/s611998005/s611998005Form',{
    data:data
  });
};

module.exports = controller;
