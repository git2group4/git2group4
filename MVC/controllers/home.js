const controller = {};
controller.home = (req, res) => {
    res.render("home", { session: req.session });
};
module.exports = controller;