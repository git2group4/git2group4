
const controller = {};
const { validationResult } = require('express-validator');


controller.list = (req,res) => {
  req.getConnection((err,conn) => {
    conn.query('SELECT *  FROM (SELECT * FROM tb19) as x JOIN (SELECT DATE_FORMAT(d611998019,"%Y-%m-%d") as time FROM tb19) as y GROUP BY id;',(err,xx) => {

      if(err){
        res.json(err);
      }
    //  console.log(customers);  //ไม่เอามาก้อดั้ย
      res.render('s611998019/index',{
        data:xx,
        session: req.session
      });
    });
  });
};

controller.add = (req,res) => {
  const data=req.body;
  const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/s611998019/new');
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('INSERT INTO tb19 set ?',[data],(err,xx) =>{
          if(err){
            res.json(err);
          }

          console.log(xx);
          res.redirect('/s611998019');
        });
      });
    }
};

controller.save =(req,res) =>{
  console.log(req.body);
  const data=req.body;
  req.getConnection((err,conn) =>{
    conn.query('INSERT INTO tb19 set ?',[data],(err,xx) =>{
      if(err){
        res.json(err);
      }

      console.log(xx);
      res.redirect('/s611998019');
    });
  });
};


controller.delete =(req,res) =>{
  const {id} =req.params;
  req.getConnection((err,conn) =>{
    conn.query('DELETE FROM tb19 WHERE id = ?',[id],(err,xx) =>{
      if(err){
        res.json(err);
      }
      console.log(xx);
      res.redirect('/s611998019');
    });
  });
};

controller.del =(req,res) =>{
  const {id} =req.params;
  req.getConnection((err,conn) =>{
    conn.query('SELECT *  FROM (SELECT * FROM tb19) as x JOIN (SELECT DATE_FORMAT(d611998019,"%Y-%m-%d") as time FROM tb19) as y GROUP BY id HAVING id = ?',[id],(err,xx) =>{
      if(err){
        res.json(err);
      }
      console.log(xx);

      res.render('s611998019/formDelete',{
              data:xx[0],
              session: req.session

      });
    });
  });
};



controller.edit =(req,res) =>{
  const {id} =req.params;
  req.getConnection((err,conn) =>{
    conn.query('SELECT *  FROM (SELECT * FROM tb19) as x JOIN (SELECT DATE_FORMAT(d611998019,"%Y-%m-%d") as time FROM tb19) as y GROUP BY id HAVING id = ?',[id],(err,xx) =>{
      if(err){
        res.json(err);
      }
      console.log(xx);
      res.render('s611998019/form',{
              data:xx[0],
              session: req.session
      });
    });
  });
};

controller.up = (req,res) => {
  const errors = validationResult(req);
  const {id} =req.params;
  const data=req.body;
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      req.getConnection((err,conn) =>{
        conn.query('SELECT *  FROM (SELECT * FROM tb19) as x JOIN (SELECT DATE_FORMAT(d611998019,"%Y-%m-%d") as time FROM tb19) as y GROUP BY id HAVING id = ?',[id],(err,xx) =>{
          if(err){
            res.json(err);
          }
          console.log(xx);
          res.render('s611998019/form',{
                  data:xx[0],
                  session: req.session
          });
        });
      });
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('UPDATE tb19 SET ? WHERE id = ?',[data,id],(err,xx) =>{
          if(err){
            res.json(err);
          }
          console.log(xx);
          res.redirect('/s611998019');
        });
      });
    }
};

controller.update =(req,res) =>{
  const {id} =req.params;
  const data=req.body;
  req.getConnection((err,conn) =>{
    conn.query('UPDATE tb19 SET ? WHERE id = ?',[data,id],(err,xx) =>{
      if(err){
        res.json(err);
      }
      console.log(xx);
      res.redirect('/s611998019');
    });
  });
};

controller.new =(req,res) =>{
  const data=null;
  res.render('s611998019/form',{
    data:data,
    session: req.session
  });
};


module.exports = controller;
