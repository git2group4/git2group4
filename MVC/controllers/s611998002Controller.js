const controller = {};
const { validationResult } = require('express-validator');
const { default: validator } = require('validator');
controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM tb02", (err, s611998002) => {
            if (err) {
                res.json(err);
            }
            res.render('../views/s611998002/s611998002', {
                data: s611998002,
                session: req.session
            });
        });
    });
};
controller.add = (req, res) => {
    const data = req.body;
    const errors = validationResult(req);
    req.getConnection((err, conn) => {
        conn.query('insert into tb02 set ?', [data], (err, s611998002) => {
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                res.redirect('/bas/new')
            } else {
                req.session.success = true;
                req.session.topic = "เพิ่มข้อมูลสำเร็จ";
                console.log('../views/s611998002/s611998002');
                res.redirect('/bas');
            };
        });
    });
};
controller.delete = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('delete from tb02 where id = ?', [id], (err, s611998002) => {
            if (err) {
                res.json(err);
            }
            console.log('../views/s611998002/s611998002');
            res.redirect('/bas');
        });
    });
};
controller.edit = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from tb02 where id = ?', [id], (err, s611998002) => {
            if (err) {
                res.json(err);
            }
            res.render('../views/s611998002/s611998002Form', {
                data: s611998002[0],
                session: req.session
            });
        });
    });
};
controller.update = (req, res) => {
    const { id } = req.params;
    const data = req.body;
    const errors = validationResult(req);
    req.getConnection((err, conn) => {
        conn.query('update tb02 set ? where id = ?', [data, id], (err, s611998002) => {
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                res.redirect('/bas/update/:id')
            } else {
                req.session.success = true;
                req.session.topic = "อัพเดทข้อมูลสำเร็จ";
                console.log('../views/s611998002/s611998002');
                res.redirect('/bas');
            };
        });
    });
};
controller.new = (req, res) => {
    const data = null;
    res.render('../views/s611998002/s611998002Form', {
        data: data,
        session: req.session
    });
};

module.exports = controller;