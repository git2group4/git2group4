const express = require('express');
const router = express.Router();

const oatController = require('../controllers/tb03Controller');

router.get('/oat',oatController.list);
router.post('/oat/add',oatController.save);
router.get('/oat/delete/:id',oatController.delete);
router.get('/oat/update/:id',oatController.edit);
router.post('/oat/update/:id',oatController.update);
router.get('/oat/new',oatController.new);

module.exports = router;
