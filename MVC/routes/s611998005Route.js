const express = require('express');
const router = express.Router();

const customerController = require ('../controllers/s611998005Controller');

router.get('/pram' , customerController.list);
router.post('/pram/add' , customerController.save);
router.get('/pram/delete/:id', customerController.delete);
router.get('/pram/update/:id', customerController.edit);
router.post('/pram/update/:id', customerController.update);
router.get('/pram/new', customerController.new);

module.exports = router;